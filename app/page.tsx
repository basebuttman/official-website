import Image from "next/image";

export default function Home() {
  return (
    <main className="font-content flex min-h-screen flex-col items-center justify-between p-12" style={{background: 'rgba(0,0,0,.4)'}}>

      <div className="relative flex place-items-center m-8">
        <Image
          className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
          src={"/basebuttman.png"}
          alt="Logo"
          width={400}
          height={400}
          priority
        />
      </div>

      <div className="text-white pb-20" style={{textShadow: 'black 0.1rem 0.1rem 0.1rem'}}>
        <p className="text-center text-3xl md:text-5xl m-5 font-title">The website is under Construction</p>
        <p className="text-center text-xl md:text-3xl m-5">Community takeover</p>
      </div>

      <div className="font-footer text-white" style={{textShadow: 'black 0.1rem 0.1rem 0.1rem'}}>
      <p className="text-center text-lg md:text-xl">© Base Buttman 2024</p>
      </div>
    </main>
  );
}
